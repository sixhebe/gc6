## The Go Challenge 6: Daedalus & Icarus

The details of the challenge are given 
[at the Go Challenge page](http://golang-challenge.com/go-challenge6/),
so I won't repeat them here.  Instead, I'll discuss my strategies
for Daedalus and Icarus.

### General remarks

There are several points that affect the overall strategy of both
players.

- Icarus and the wings are placed randomly.

- When Daedalus designs the maze, he has no
foreknowledge of where Icarus and the wings
will be placed (this important point was clarified by @spf13 in
the Slack channel), 

- When Icarus wakes up, he doesn't know where in the maze he is, and
he doesn't know where the wings are.  He doesn't even know the size
of the maze (though in the Slack channel, @spf13 said that we could
assume a 10x15 maze).

- There are no lights in the maze, and Icarus does not have a lantern.  In 
other words, even if the maze has no internal walls, 
the only way Icarus can
find the wings is by traversing all the rooms, 
one by one.  He can't see the wings glittering in the distance;
he can't even detect whether they are in the next room.

- Daedalus and Icarus play the "maze game" multiple times (500
times, for
purposes of scoring the contest), and can remember the
outcome from one game to the next.  So Daedalus can observe which
mazes Icarus finds most difficult, and give Icarus more of those.
And Icarus could potentially develop a search strategy based on the
kinds of mazes that Daedalus is observed to create.

### Icarus

Given all the above, Icarus's strategy is pretty straighforward.  

Icarus needs
to make a mental map of the maze, keeping track of which rooms he
has visited, and where the walls are.  

Icarus also needs to keep track
of rooms he has "glimpsed": rooms that he *hasn't* visited, but whose
existence he has inferred from the absence of walls in the rooms he
*has* visited.  For instance, if he visits a particular room, and notices
that this room has no wall to the north, he can infer the presence of
a room to the north, even if he doesn't visit that room at this time.
The room to the north is "glimpsed", in my terminology.

When Icarus is in a given room, he chooses the next room to go to
according to the following list of priorities (top priority is at 
the top of the list).

- Don't try to walk through walls.
- Don't revisit rooms that have already been visited.
- If neither of these is possible, use the mental map of the
maze to compute the shortest path to the nearest glimpsed but unvisited
room.
- If there is more than one room that meets the above criteria,
choose the room by its direction, 
according to this list (highest to lowest priority): 
N, S, E, W.

The last criterion is important because it ensures that in an empty
maze (one with no internal walls), Icarus performs a systematic scan
rather than a random walk.

The above strategy should work efficiently in all mazes, 
including
mazes with no internal walls.  Of course, it won't work in improperly
built mazes, such as ones with one-way walls or 
with inaccessible rooms.  And if Icarus had more information, such
as where the wings are placed relative to his
current location, then a different strategy would
be preferred.

### Daedalus

Because Daedalus doesn't know where Icarus and the wings will be placed,
he has to construct a maze that will work well for any placement.

Even an empty maze (one with no internal walls) will work fairly well,
because Icarus can't see the wings from a distance; he has to traverse
the maze cell by cell.  And an empty maze would defeat versions of Icarus
that use a wall-following algorithm (put your right hand on the wall,
and keep your hand on the wall as you traverse the maze).

But assuming that Icarus is reasonably clever, the goal must be to make
him backtrack as much as possible.  It turns out that a complicated maze
is not necessary for this purpose; a simple comb shape will do.  The
only reason for a more complicated maze would be if Icarus failed to
keep a mental map of the maze; in that case a complicated maze would
confuse Icarus and cause him to backtrack more than a simple maze would.

A simple comb-shaped maze:

```
┌────────────────────────────────────────────┐
│                                            │
│                                            │
│  │  │  │  │  │  │  │  │  │  │  │  │  │  │  │
│  │  │  │  │  │  │  │  │  │  │  │  │  │  │  │
│  │  │  │  │  │SS│  │  │  │  │  │  │  │  │  │
│  │  │  │  │  │  │  │  │  │  │  │  │  │  │  │
│  │  │  │  │  │  │  │  │  │ww│  │  │  │  │  │
│  │  │  │  │  │  │  │  │  │  │  │  │  │  │  │
│  │  │  │  │  │  │  │  │  │  │  │  │  │  │  │
│  │  │  │  │  │  │  │  │  │  │  │  │  │  │  │
│  │  │  │  │  │  │  │  │  │  │  │  │  │  │  │
│  │  │  │  │  │  │  │  │  │  │  │  │  │  │  │
│  │  │  │  │  │  │  │  │  │  │  │  │  │  │  │
│  │  │  │  │  │  │  │  │  │  │  │  │  │  │  │
│  │  │  │  │  │  │  │  │  │  │  │  │  │  │  │
│  │  │  │  │  │  │  │  │  │  │  │  │  │  │  │
│  │  │  │  │  │  │  │  │  │  │  │  │  │  │  │
│  │  │  │  │  │  │  │  │  │  │  │  │  │  │  │
│  │  │  │  │  │  │  │  │  │  │  │  │  │  │  │
└──┴──┴──┴──┴──┴──┴──┴──┴──┴──┴──┴──┴──┴──┴──┘
```

Since Icarus and Daedalus compete multiple times in a single
session, Daedalus could in theory determine which mazes give
Icarus the most difficulty, and provide more of these 
mazes. I did not try to implement this.

Just for fun, I programmed Daedalus to create some more "interesting"
(from a human point of view) mazes, in addition to the comb mazes.  By
default, Daedalus only creates the comb mazes, since these are the
most effective in competition.  But if the user puts the `-i` (for
"interesting") flag on the command line, then other mazes (depth-first,
breadth-first, serrated-comb) will be generated as well.  Use the
options `-v=t -i -t 8` to view all of them.  (The `-v=t` option is
described below).


### Results

When I ran some tests of my Icarus versus my Daedalus,
I got an average of about 80 steps in an empty maze (one with
no internal walls).  And in a comb maze (as shown above),
I got an average of about 135 steps.  I haven't done an
analysis to see if this is optimal.

### Things that could go wrong

There are various programming errors that could affect the final
score.  

For instance, as mentioned above, if Daedalus incorrectly 
creates one-way walls or inaccessible rooms, the maze may be
unsolvable.  I implemented some consistency checks to verify that my
version of Daedalus doesn't create such mazes.

Another thing that could go wrong is with the `-max-steps` flag.

Suppose that Icarus is in charge of making sure he doesn't
exceed `max-steps` steps, and that he breaks out of the solver
when he exceeds that limit.  (This is how I implemented it).

Daedalus, the way he is currently programmed, only records maze runs
that result in a victory.  If Icarus simply gives up on a maze before
finding the wings, and requests a new maze (the `awake` API command),
Daedalus throws away the results of the unsuccessful run and starts
over.  

Thus one way Icarus could ensure a good score is to set `max-steps` to
a fairly low number (say 50).  That would only record scores when Icarus
by chance starts pretty close to the wings.  And to make sure he notches
up the required number of victories, Icarus can just keep trying.  Daedalus
won't object, and won't record the failed attempts.

### Live display

I was interested in learning how to use 
[termbox](https://github.com/nsf/termbox-go),
a go-based version of ncurses.  So I spent a lot of time figuring out
how to get `termbox`
to show, in real time, Icarus solving the maze.  If Daedalus
and Icarus are sharing the same terminal, only Daedalus's view is shown.
If Daedalus and Icarus are running on separate terminals, then
Icarus's incomplete mental map is displayed, as well as 
Daedalus's "God view".

These display are controlled by the `-v` (for verbose) flag; `-v=t`
turns on the termbox view.  Note that when the termbox view
is selected, the program introduces a small pause after each step, 
so that the user can more easily keep track of Icarus's moves.

### Libraries

As just mentioned, my version of the code uses the `termbox`
library.  The code we were given also uses the libraries 
`gin`, `cobra`, and `viper`.  In my code, I modifed the `gin` calls
in order to take control of the session logging (the default logging
would have conflicted with the `termbox` display).  I also added some `cobra`
code in order to add new command-line parameters (`-v` to control 
output verbosity, and `-i` to create interesting, though non-competitive,
mazes).

### Housekeeping

I used `gofmt`, `go vet`, and `golint` to keep the code neat and tidy.

### Further work

I focused so much on termbox, that I didn't spend as much time as I
would have liked designing maze-generation algorithms.  I added a
couple just for fun (breadth-first and depth-first; see comments in
`daedalus.go` for details), but I would have liked to
program up some more.  However, I'm not convinced
that any maze would be better than the comb mazes that I currently
use.

I would have liked to make Daedalus adaptive, to take
advantage of any weaknesses observed in Icarus's maze-solving
algorithm.  For instance, if Icarus had trouble with empty mazes (mazes
without internal walls), then Daedalus might have adapted to
serve Icarus more empty mazes, instead of the standardized comb mazes. 

I would have liked to have made Icarus's mental map of the maze a bit
more general.  At the moment, this maze has a fixed size sufficient
to handle a 10x15 maze.  But it would be nice to make the maze dynamic,
so it would automatically resize whenever Icarus discovered that the maze
was bigger than he expected.

I would have liked for Icarus to perform some consistency checks,
to detect whether Daedalus had created a non-conforming maze (one
with one-way walls and/or inaccessible rooms).  And it would have
been nice to modify Daedalus's scoring algorithm to prevent Icarus
from benefitting by setting `-max-steps` too low.

But at some point, one has to stop.

Anyway, thanks for the interesting challenge!
