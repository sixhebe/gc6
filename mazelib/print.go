// Copyright © 2015 Steve Francia <spf@spf13.com>.
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//

// Package mazelib is a small set of interfaces and utilities designed to help
// with the Go Challenge 6: Daedalus & Icarus
package mazelib

import (
	"bytes"
	"errors"
	"fmt"
	"github.com/nsf/termbox-go"
	"io"
	"os"
	"runtime"
	"strings"
	"time"
)

// PrintMazeOld : Function to Print Maze to Console
func PrintMazeOld(m MazeI) {
	fmt.Println("_" + strings.Repeat("___", m.Width()))
	for y := 0; y < m.Height(); y++ {
		str := ""
		for x := 0; x < m.Width(); x++ {
			if x == 0 {
				str += "|"
			}
			r, err := m.GetRoom(x, y)
			if err != nil {
				fmt.Println(err)
				os.Exit(-1)
			}
			s, err := m.Discover(x, y)
			if err != nil {
				fmt.Println(err)
				os.Exit(-1)
			}
			if s.Bottom {
				if r.Treasure {
					str += "W_"
				} else if r.Start {
					str += "I_"
				} else {
					str += "__"
				}
			} else {
				if r.Treasure {
					str += "W "
				} else if r.Start {
					str += "I "
				} else {
					str += "  "
				}
			}

			if s.Right {
				str += "|"
			} else {
				str += "_"
			}

		}
		fmt.Println(str)
	}
}

var bb = `
└ 3
│ 5
┌ 6
├ 7
┘ 9
─ a
┴ b
┐ c
┤ d
┬ e
┼ f
`

// Routines for generating an ascii-art display of the maze.
// Uses the unicode box characters.
//
// Imagine that you are at the lower right corner of a room int the maze.
// How would you draw this corner, given that you know all of the nearby
// walls?  You'd start, perhaps, with an invisible dot in the middle.  Then,
// if there is a wall between you and the room on your right, you'd draw a
// little line upwards from the dot.  If there were also a wall between you
// and the room beneath you, you'd draw another little line to the left
// from the dot.  You would end up with something like this character: ┘
// If you also knew that the room beneath you had a wall to the right, you'd
// another little line: ┤
// And if you also knew that the room to your right had a wall on its
// south side, you'd finally end up with: ┼

// wallCharBytes lists the different unicode box characters, sorted
// according to a simple scoring scheme.  Start with zero.  If the
// unicode glyph has a line from the center to the north, add 1.
// If it has a line from the center to the east, add 2.
// If it has a line from the center to the south, add 4.
// If it has a line from the center to the west, add 8.
// The resulting score gives the glyph's position in wallCharBytes.
// Notice that a score of 0 is represented by a space (" ").
var wallCharBytes = []byte(" ╵╶└╷│┌├╴┘─┴┐┤┬┼")

// We will work with unicode runes, not bytes.
var wallCharRunes = bytes.Runes(wallCharBytes)
var visitedRune = ':'
var icarusRune = '@'

/*
Layout of walls (as described above):

      1
	 8 2
	  4
*/

func init() {
	// The windows fonts that are available to the windows console are
	// missing some of the unicode box symbols.  Replace the missing
	// symbols by spaces.
	if runtime.GOOS == "windows" {
		wallCharRunes[1] = ' '
		wallCharRunes[2] = ' '
		wallCharRunes[4] = ' '
		wallCharRunes[8] = ' '
	}
}

// dx, dy, wdir, odir
// Given another room at location dx,dy relative to the current
// room, and given the presence of a wall on side wdir of that room,
// figure out the direction of the wall that should be drawn.
// For instance, in the current room, dx,dy = 0,0, and a wall on the
// the east side of the current room should be represented by a line
// to the north.  That is, {0, 0, E}: N.
var cornerDir2Dir = map[[3]int]int{
	[3]int{0, 0, E}: N,
	[3]int{0, 0, S}: W,
	[3]int{1, 0, W}: N,
	[3]int{1, 0, S}: E,
	[3]int{0, 1, E}: S,
	[3]int{0, 1, N}: W,
	[3]int{1, 1, W}: S,
	[3]int{1, 1, N}: E,
}

// N->1, S->4, E->2, W->8
var dirFlag = []int{0, 1, 4, 2, 8}

// Given a room at x,y, determine the wallFlags (the "score" discussed
// in the above comments) around its lower right corner.
// The wallFlags determine which unicode box character will
// be used to draw this corner.
// Also figure out whether the 4 rooms around this corner have been
// visited, and store this information in visitedFlags.
// This algorithm correctly handles the case where some of the
// neighboring rooms don't exist (they may be outside the maze,
// for instance).
//
func wallsAtLowerRightCorner(m MazeI, x, y int) (wallFlags int, visitedFlags int) {
	for dx := 0; dx < 2; dx++ {
		for dy := 0; dy < 2; dy++ {
			r, err := m.GetRoom(x+dx, y+dy)
			if err != nil {
				continue
			}
			if r.Visited {
				visitedFlags |= 1 << uint(dx+2*dy)
			}
			sa := NewSArray(&r.Walls)
			for _, dir := range NSEW {
				if !sa.Walls[dir] {
					continue
				}
				odir := cornerDir2Dir[[3]int{dx, dy, dir}]
				wallFlags |= dirFlag[odir]
			}
		}
	}
	return wallFlags, visitedFlags
}

// PrintMaze prints an entire maze (MazeI).
func PrintMaze(w io.Writer, m MazeI) {
	printMazeInRange(w, m, 0, 0, m.Width(), m.Height())
}

// PrintMazeRoom prints a single room of a maze (MazeI).
func PrintMazeRoom(w io.Writer, m MazeI, x, y int) {
	printMazeInRange(w, m, x, y, 1, 1)
}

// These correspond to output cell sizes in printMazeInRange
var outputCellWidth = 3
var outputCellHeight = 2

func printMazeInRange(w io.Writer, m MazeI, x0, y0, dx, dy int) {
	icarusX, icarusY := m.Icarus()
	for y := y0 - 1; y < y0+dy; y++ {
		var str0 string
		var str string
		for x := x0 - 1; x < x0+dx; x++ {
			c, v := wallsAtLowerRightCorner(m, x, y)
			_ = v
			if x > x0-1 {
				var r rune
				if c&8 == 8 {
					r = wallCharRunes[0xa]
				} else if v&5 == 5 {
					r = visitedRune
				} else {
					r = ' '
				}
				str += string(r) + string(r)
			}
			r := wallCharRunes[c]
			if c == 0 && v == 0xf {
				r = visitedRune
			}
			str += string(r)
			var r0 rune
			if c&1 == 1 {
				r0 = wallCharRunes[5]
			} else if v&3 == 3 {
				r0 = visitedRune
			} else {
				r0 = ' '
			}
			str0 += string(r0)
			if x < x0+dx-1 {
				room, err := m.GetRoom(x+1, y)
				if err != nil {
					str0 += "  "
				} else if room.Visited {
					if room.Start {
						str0 += "SS"
					} else if room.Treasure {
						str0 += "WW"
					} else {
						if x+1 == icarusX && y == icarusY {
							str0 += string(icarusRune) + string(icarusRune)
						} else {
							str0 += string(visitedRune) + string(visitedRune)
						}
					}
				} else {
					if room.Start {
						str0 += "ss"
					} else if room.Treasure {
						str0 += "ww"
					} else {
						str0 += "  "
					}
				}
			}
		}
		if y > y0-1 {
			fmt.Fprintln(w, str0)
		}
		fmt.Fprintln(w, str)
	}
}

// ParseVerboseFlag takes an input string (from the command line)
// and converts it to a single letter.  'n' means non-verbose (quiet);
// 'p' means print (ordinary level of verbosity); 't' means use
// the termbox routines to display the maze on the console.
// Since this routine is used only by icarus and daedalus, perhaps
// it could be moved to the commands package.
func ParseVerboseFlag(str string) (r rune) {
	r = 'n'
	if len(str) == 0 {
		return r
	}
	runes := bytes.Runes([]byte(str))
	r0 := runes[0]
	if r0 == 't' || r0 == 'p' {
		r = r0
	}
	return r
}

// PrintController manages the verbosity/termbox settings.
// The daedalus and icarus commands need separate instances
// of PrintController, since they may be operating at different
// levels of verbosity.  For instance, if daedalus and icarus
// share the same console, only one can be in termbox mode.
type PrintController struct {
	verbose            bool
	termboxInited      bool
	shift              Coordinate
	graphicsAreaHeight int
	fg                 termbox.Attribute
	bg                 termbox.Attribute
}

// SetVerbose sets whether the printing is verbose.
func (pc *PrintController) SetVerbose(val bool) {
	pc.verbose = val
}

// Printf is designed to act like fmt.Printf.
// However, if verbosity is turned off, this function does nothing.
// And if termbox mode is turned on, the printing is passed to
// the termbox console for output.
func (pc *PrintController) Printf(fstr string, a ...interface{}) {
	if !pc.verbose {
		return
	}
	if !pc.termboxInited {
		fmt.Printf(fstr, a...)
		return
	}
	str := strings.TrimSpace(fmt.Sprintf(fstr, a...))
	pc.stringToTermbox(str)
}

// Println is designed to act like fmt.Println.
// However, if verbosity is turned off, this function does nothing.
// And if termbox mode is turned on, the printing is passed to
// the termbox console for output.
func (pc *PrintController) Println(a ...interface{}) {
	if !pc.verbose {
		return
	}
	if !pc.termboxInited {
		fmt.Println(a)
		return
	}
	str := strings.TrimSpace(fmt.Sprintln(a...))
	pc.stringToTermbox(str)
}

// IsVerbose returns whether verbosity is turned on.
// In theory, no one outside of the mazelib package should care,
// since PrintController keeps track of what to print and not print.
// However, in practice it might be helpful for optimization (don't
// even try to print if verbosity is turned off)
func (pc *PrintController) IsVerbose() bool {
	return pc.verbose
}

// IsTermboxInited returns whether we are in termbox mode.
// In theory, no one outside of the mazelib package should care,
// since PrintController keeps track of what to print and not print.
// However, in practice it might be helpful for optimization (don't
// even try to print if termbox is turned off)
func (pc *PrintController) IsTermboxInited() bool {
	return pc.termboxInited
}

// OpenTermbox attempts to open termbox on the console. It also
// turns on verbosity mode.  If termbox.Init fails, termbox mode
// is not turned on.
func (pc *PrintController) OpenTermbox() error {
	pc.verbose = true
	if pc.termboxInited {
		return errors.New("OpenTermbox: already inited")
	}
	err := termbox.Init()
	if err != nil {
		return err
	}
	pc.termboxInited = true
	pc.graphicsAreaHeight = 22
	pc.fg = termbox.ColorDefault
	pc.bg = termbox.ColorDefault
	return nil
}

// BreakIfTermboxInterrupt (or NotifyIfTermboxInterrupt) must
// be called by the client just after OpenTermbox.  This is to
// prevent hanging on exit.
func (pc *PrintController) BreakIfTermboxInterrupt() {
	if !pc.termboxInited {
		return
	}
	// apparently, if termbox.PollEvent() is never called, termbox.Close()
	// won't close properly
	go func() {
		for {
			ev := termbox.PollEvent()
			if ev.Type == termbox.EventKey && ev.Key == termbox.KeyCtrlC {
				pc.CloseTermbox()
				os.Exit(1)
			}
		}
	}()
}

// NotifyIfTermboxInterrupt (or BreakIfTermboxInterrupt) must
// be called by the client just after OpenTermbox.  This is to
// prevent hanging on exit.  The channel is used to signal
// daedalus in case of a ctrl-C interrupt, so that daedalus can
// print the final score before exiting.
func (pc *PrintController) NotifyIfTermboxInterrupt(c chan os.Signal) {
	if !pc.termboxInited {
		return
	}
	go func(c chan os.Signal) {
		for {
			ev := termbox.PollEvent()
			if ev.Type == termbox.EventKey && ev.Key == termbox.KeyCtrlC {
				c <- os.Interrupt
			}
		}
	}(c)
}

// CloseTermbox must be called before exiting the program.  If
// not in termbox mode, this call is ignored.  The Sleep command
// is there to give the user a chance to admire the final maze.
func (pc *PrintController) CloseTermbox() {
	if pc.termboxInited {
		pc.termboxInited = false
		// time.Sleep(3 * time.Second)
		termbox.Close()
	}
}

// TermboxSleep sleeps for the given time.Duration, but only
// when in termbox mode.  This function is ignored otherwise.
func (pc *PrintController) TermboxSleep(d time.Duration) {
	if pc.termboxInited {
		time.Sleep(d)
	}
}

// SetShift sets an x,y shift that is applied while printing the
// maze.  Returns true if the shift changed.
func (pc *PrintController) SetShift(c Coordinate) (changed bool) {
	if pc.shift.X != c.X || pc.shift.Y != c.Y {
		changed = true
	}
	pc.shift = c
	return changed
}

// PrintMazeRoom prints a single maze of the room on the termbox terminal,
// if in termbox mode.  Otherwise it does nothing.
// The Sleep call is there to slow down the action enough so that
// the user can perceive it.
// func (pc *PrintController) PrintMazeRoom(m MazeI, x, y int, pause bool) {
func (pc *PrintController) PrintMazeRoom(m MazeI, x, y int) {
	if !pc.termboxInited {
		return
	}
	buf := new(bytes.Buffer)
	PrintMazeRoom(buf, m, x, y)
	pc.byteBufferToTermbox(buf, x, y)
	// if pause {
	// 	time.Sleep(20 * time.Millisecond)
	// }
}

func (pc *PrintController) graphicsAreaSize() (w, h int) {
	w, h = termbox.Size()
	if pc.graphicsAreaHeight < h {
		h = pc.graphicsAreaHeight
	}
	return w, h
}

func (pc *PrintController) clearTermboxGraphicsArea() {
	if !pc.termboxInited {
		return
	}
	w, h := pc.graphicsAreaSize()
	for x := 0; x < w; x++ {
		for y := 0; y < h; y++ {
			termbox.SetCell(x, y, ' ', pc.fg, pc.bg)
		}
	}
}

// PrintMaze prints the maze on the termbox console, if in termbox
// mode.  In verbose mode, it prints the maze on the console, but
// truncated.
func (pc *PrintController) PrintMaze(m MazeI) {
	if !pc.verbose {
		return
	}
	buf := new(bytes.Buffer)
	PrintMaze(buf, m)
	if !pc.termboxInited {
		// The code in this if block should be split out
		// into a separate func.
		lines := strings.Split(buf.String(), "\n")
		for i, l := range lines {
			y := i + outputCellHeight*(-pc.shift.Y)
			if y < 0 || y > 25 {
				continue
			}
			var ostr string
			j := 0
			for _, r := range l {
				x := j + outputCellWidth*(-pc.shift.X)
				j++
				if x < 0 || x >= 75 {
					continue
				}
				ostr += string(r)
			}
			fmt.Println(ostr)
		}
		return
	}
	pc.clearTermboxGraphicsArea()
	pc.byteBufferToTermbox(buf, 0, 0)
}

func (pc *PrintController) stringToTermbox(str string) {
	w, gh := pc.graphicsAreaSize()
	_, h := termbox.Size()
	cells := termbox.CellBuffer()
	for y := h - 1; y > gh; y-- {
		for x := 0; x < w; x++ {
			oldCell := cells[x+(y-1)*w]
			termbox.SetCell(x, y, oldCell.Ch, oldCell.Fg, oldCell.Bg)
		}
	}
	for x := 0; x < w; x++ {
		runes := bytes.Runes([]byte(str))
		var r rune
		if x < len(runes) {
			r = runes[x]
		} else {
			r = ' '
		}
		termbox.SetCell(x, gh, r, pc.fg, pc.bg)
	}
	err := termbox.Flush()
	if err != nil {
		pc.CloseTermbox()
		fmt.Println("stringToTermbox error", err)
	}
}

func (pc *PrintController) byteBufferToTermbox(buf *bytes.Buffer, dx, dy int) {
	w, h := pc.graphicsAreaSize()
	lines := strings.Split(buf.String(), "\n")
	// pc.Println("bbt", pc.shift.X, pc.shift.Y, len(lines), len(lines[0]))
	for i, l := range lines {
		y := i + outputCellHeight*(-pc.shift.Y+dy)
		if y < 0 || y >= h {
			continue
		}
		// recall that first return value in range l will be
		// offset into byte string, not counter.
		// So keep track of char count with j instead
		j := 0
		for _, r := range l {
			x := j + outputCellWidth*(-pc.shift.X+dx)
			j++
			if x < 0 || x >= w {
				continue
			}
			termbox.SetCell(x, y, r, pc.fg, pc.bg)
		}
	}
	err := termbox.Flush()
	if err != nil {
		pc.CloseTermbox()
		fmt.Println("MazeToTermbox error", err)
	}
}
