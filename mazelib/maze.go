// Copyright © 2015 Steve Francia <spf@spf13.com>.
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//

// This is a small set of interfaces and utilities designed to help
// with the Go Challenge 6: Daedalus & Icarus

// Package mazelib contains general-purpose routines for dealing
// with mazes that comply with the MazeI interface (defined here).
// It includes printing routines that use termbox-go to make nice
// realtime ascii-art mazes.
package mazelib

import (
	"errors"
	"fmt"
	"math/rand"
)

// Coordinate describes a location in the maze
type Coordinate struct {
	X int `json:"x"`
	Y int `json:"y"`
}

// Reply from the server to a request
type Reply struct {
	Survey  Survey `json:"survey"`
	Victory bool   `json:"victory"`
	Message string `json:"message"`
	Error   bool   `json:"error"`
}

// Survey Given a location, survey surrounding locations
// True indicates a wall is present.
type Survey struct {
	Top    bool `json:"top"`
	Right  bool `json:"right"`
	Bottom bool `json:"bottom"`
	Left   bool `json:"left"`
}

// N, S, E, W are the cardinal directions North, South, East, West.
// Note that N=1, so to iterate over the four directions need
// something like i:=1; i<5.  Or better, use NSEW below.
const (
	N = 1
	S = 2
	E = 3
	W = 4
)

// NSEW is a convenience array for iterating over the 4 cardinal directions
var NSEW = [4]int{N, S, E, W}

// ErrVictory is the "error" produced when Icarus reaches his goal.
var ErrVictory = errors.New("Victory")

// Room contains the minimum informaion about a room in the maze.
type Room struct {
	Treasure bool
	Start    bool
	Visited  bool
	Walls    Survey
}

// AddWall adds a wall in the given direction.  Note that
// this is an UNSAFE operation because it does not maintain
// consistency between the given room and its neighbors (it
// may create a "one-way" wall)
func (r *Room) AddWall(dir int) {
	switch dir {
	case N:
		r.Walls.Top = true
	case S:
		r.Walls.Bottom = true
	case E:
		r.Walls.Right = true
	case W:
		r.Walls.Left = true
	}
}

// RmWall removes the wall in the given direction.  Note that
// this is an UNSAFE operation because it does not maintain
// consistency between the given room and its neighbors (it
// may lead to the creation of a "one-way" wall)
func (r *Room) RmWall(dir int) {
	switch dir {
	case N:
		r.Walls.Top = false
	case S:
		r.Walls.Bottom = false
	case E:
		r.Walls.Right = false
	case W:
		r.Walls.Left = false
	}
}

// MazeI Interface
type MazeI interface {
	GetRoom(x, y int) (*Room, error)
	Width() int
	Height() int
	SetStartPoint(x, y int) error
	SetTreasure(x, y int) error
	LookAround() (Survey, error)
	Discover(x, y int) (Survey, error)
	Icarus() (x, y int)
	MoveLeft() error
	MoveRight() error
	MoveUp() error
	MoveDown() error
}

// AvgScores computes the average of the given slice of scores.
func AvgScores(in []int) int {
	if len(in) == 0 {
		return 0
	}

	total := 0

	for _, x := range in {
		total += x
	}
	return total / (len(in))
}

// SArray stands for "Survey Array".  It contains the same information
// as the Survey structure, but in an array form.  This is more convenient
// for some algorithms.  The index of the array corresponds to the
// N, S, E, W values.  Note that the index should never be 0, since that
// doesn't correspond to any of the four cardinal directions.
type SArray struct {
	Walls [5]bool
}

// RandomUnwalledDirection return a random direction (N,S,E,W)
// that, according to the SArray, is unwalled.  If there are walls
// in all directions, RandomUnwalledDirection returns 0.
func (sa *SArray) RandomUnwalledDirection() int {
	dirs := make([]int, 0, 5)
	// count number of walls where Walls[i] = false
	for _, i := range NSEW {
		if !sa.Walls[i] {
			dirs = append(dirs, i)
		}
	}
	count := len(dirs)
	if count == 0 {
		return 0
	}
	dir := rand.Int31n(int32(count))
	return dirs[dir]
}

// FirstUnwalledDirection returns a direction (N,S,E,W)
// that, according to the SArray, is unwalled.  If there are walls
// in all directions, FirstUnwalledDirection returns 0.
// If there is more than one unwalled direction to choose from,
// examine the nsBias (north-south bias) flag.  If
// nsBias is true, search in the order N, S, E, W.  Otherwise,
// search in the order E, W, N, S.
func (sa *SArray) FirstUnwalledDirection(nsBias bool) int {
	ns := []int{N, S, E, W}
	ew := []int{E, W, N, S}
	var dirs []int
	if nsBias {
		dirs = ns
	} else {
		dirs = ew
	}
	for _, dir := range dirs {
		if !sa.Walls[dir] {
			return dir
		}
	}
	return 0
}

// NewSArray creates an SArray from a Survey.
func NewSArray(s *Survey) SArray {
	var sa SArray
	sa.Walls[N] = s.Top
	sa.Walls[S] = s.Bottom
	sa.Walls[E] = s.Right
	sa.Walls[W] = s.Left
	return sa
}

// AllTrueSArray creates an SArray with walls in all directions.
func AllTrueSArray() SArray {
	var sa SArray
	for _, i := range NSEW {
		sa.Walls[i] = true
	}
	return sa
}

// Dir2Delta is an array that returns dx,dy given a direction (N, S, E, W).
// The dx,dy values are returned inside a Coordinate.
var Dir2Delta = []Coordinate{
	{0, 0},
	{0, -1},
	{0, 1},
	{1, 0},
	{-1, 0},
}

// TrueIfWallOrVisited returns an SArray with the wall flags set
// according to a more general criterion that simply whether a
// wall exists. In each direction (N, S, E, W),
// the corresponding "Walls" flag is set to true if there is a wall
// in that direction, or if the neighboring room in that direction has
// already been visited.  So false means that the room is visitable
// (not obstructed by a wall), and has not yet been visited.
func TrueIfWallOrVisited(m MazeI, x, y int) SArray {
	r, err := m.GetRoom(x, y)
	if err != nil {
		return AllTrueSArray()
	}
	sa := NewSArray(&r.Walls)
	for _, dir := range NSEW {
		if sa.Walls[dir] {
			continue
		}
		c := Dir2Delta[dir]
		r1, err := m.GetRoom(x+c.X, y+c.Y)
		if err != nil || r1.Visited {
			sa.Walls[dir] = true
		}
	}
	return sa
}

// Dir2Move is an array that given a direction (N, S, E, W),
// returns a string containing the direction of movement that
// is used by icarus and daedalus to communicate.
var Dir2Move = [5]string{"", "up", "down", "right", "left"}

// SetOuterWalls sets (creates) the outer walls of the maze.
func SetOuterWalls(m MazeI) error {
	w := m.Width()
	h := m.Height()
	for y := 0; y < h; y++ {
		if y == 0 {
			for x := 0; x < w; x++ {
				r, err := m.GetRoom(x, y)
				if err != nil {
					return err
				}
				r.Walls.Top = true
			}
		}
		if y == h-1 {
			for x := 0; x < w; x++ {
				r, err := m.GetRoom(x, y)
				if err != nil {
					return err
				}
				r.Walls.Bottom = true
			}
		}
		r, err := m.GetRoom(0, y)
		if err != nil {
			return err
		}
		r.Walls.Left = true
		r, err = m.GetRoom(w-1, y)
		if err != nil {
			return err
		}
		r.Walls.Right = true
	}
	return nil
}

// AreOuterWallsSet checks whether the outer walls of the maze are set.
// Returns non-nil error if false.
func AreOuterWallsSet(m MazeI) (bool, error) {
	w := m.Width()
	h := m.Height()
	for y := 0; y < h; y++ {
		if y == 0 {
			for x := 0; x < w; x++ {
				r, err := m.GetRoom(x, y)
				if err != nil {
					return false, err
				}
				if !r.Walls.Top {
					return false, errors.New("Outer walls not set")
				}
			}
		}
		if y == h-1 {
			for x := 0; x < w; x++ {
				r, err := m.GetRoom(x, y)
				if err != nil {
					return false, err
				}
				if !r.Walls.Bottom {
					return false, errors.New("Outer walls not set")
				}
			}
		}
		r, err := m.GetRoom(0, y)
		if err != nil {
			return false, err
		}
		if !r.Walls.Left {
			return false, errors.New("Outer walls not set")
		}
		r, err = m.GetRoom(w-1, y)
		if err != nil {
			return false, err
		}
		if !r.Walls.Right {
			return false, errors.New("Outer walls not set")
		}
	}
	return true, nil
}

// IsTopologyConsistent checks that there are no one-way walls.
// Returns non-nil error if false.
func IsTopologyConsistent(m MazeI) (bool, error) {
	estr := "Inconsistent wall between (%d,%d) and (%d,%d)"
	w := m.Width()
	h := m.Height()
	for y := 0; y < h; y++ {
		if y != h-1 {
			for x := 0; x < w; x++ {
				r0, err := m.GetRoom(x, y)
				if err != nil {
					return false, err
				}
				r1, err := m.GetRoom(x, y+1)
				if err != nil {
					return false, err
				}
				if r0.Walls.Bottom != r1.Walls.Top {
					return false, fmt.Errorf(estr, x, y, x, y+1)
				}
			}
		}
		for x := 0; x < w-1; x++ {
			r0, err := m.GetRoom(x, y)
			if err != nil {
				return false, err
			}
			r1, err := m.GetRoom(x+1, y)
			if err != nil {
				return false, err
			}
			if r0.Walls.Right != r1.Walls.Left {
				return false, fmt.Errorf(estr, x, y, x+1, y)
			}
		}
	}
	return true, nil
}
