// Copyright © 2015 Steve Francia <spf@spf13.com>.
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//

package commands

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/sixhebe/gc6/mazelib"
)

// Defining the icarus command.
// This will be called as 'laybrinth icarus'
var icarusCmd = &cobra.Command{
	Use:     "icarus",
	Aliases: []string{"client"},
	Short:   "Start the laybrinth solver",
	Long: `Icarus wakes up to find himself in the middle of a Labyrinth.
  Due to the darkness of the Labyrinth he can only see his immediate cell and if
  there is a wall or not to the top, right, bottom and left. He takes one step
  and then can discover if his new cell has walls on each of the four sides.

  Icarus can connect to a Daedalus and solve many laybrinths at a time.`,
	Run: func(cmd *cobra.Command, args []string) {
		RunIcarus()
	},
}

func init() {
	RootCmd.AddCommand(icarusCmd)
}

var ipc mazelib.PrintController

// RunIcarus runs the solver as many times as the user desires.
func RunIcarus() {

	verboseStr := viper.GetString("verbose")
	v := mazelib.ParseVerboseFlag(verboseStr)
	// SharingConsole is set (in labyrinth.go) if daedalus and icarus
	// are sharing the same console.  In that case they are also sharing
	// the same v flag.  If v flag == 't' (use termbox), let daedalus
	// operate the termbox, and suppress all output from icarus.
	if SharingConsole && v == 't' {
		v = 'n'
	}
	if v == 'p' {
		ipc.SetVerbose(true)
	} else if v == 't' {
		ipc.SetVerbose(true)
		ipc.OpenTermbox()
		ipc.BreakIfTermboxInterrupt()
	} else {
		ipc.SetVerbose(false)
	}
	ipc.Println("Solving", viper.GetInt("times"), "times")
	for x := 0; x < viper.GetInt("times"); x++ {
		solveMaze()
		ipc.TermboxSleep(1 * time.Second)
	}

	// Once we have solved the maze the required times, tell daedalus we are done
	makeRequest("http://127.0.0.1:" + viper.GetString("port") + "/done")
	ipc.CloseTermbox()
}

// Make a call to the laybrinth server (daedalus) that icarus is ready to wake up
func awake() mazelib.Survey {
	contents, err := makeRequest("http://127.0.0.1:" + viper.GetString("port") + "/awake")
	if err != nil {
		fmt.Println("awake", err)
	}
	r := ToReply(contents)
	return r.Survey
}

// Move makes a call to the laybrinth server (daedalus)
// to move Icarus a given direction
// Will be used heavily by solveMaze
func Move(direction string) (mazelib.Survey, error) {
	if direction == "left" || direction == "right" || direction == "up" || direction == "down" {

		contents, err := makeRequest("http://127.0.0.1:" + viper.GetString("port") + "/move/" + direction)
		if err != nil {
			return mazelib.Survey{}, err
		}

		rep := ToReply(contents)
		if rep.Victory == true {
			return rep.Survey, mazelib.ErrVictory
		}
		return rep.Survey, errors.New(rep.Message)
	}

	return mazelib.Survey{}, errors.New("invalid direction")
}

// utility function to wrap making requests to the daedalus server
func makeRequest(url string) ([]byte, error) {
	response, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()
	contents, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}
	return contents, nil
}

// ToReply handles a JSON response and unmarshals it into a reply struct
func ToReply(in []byte) mazelib.Reply {
	res := &mazelib.Reply{}
	json.Unmarshal(in, &res)
	return *res
}

// Icarus's copy of the maze is larger than the actual maze,
// so it is too big to print on the terminal, and most of it
// is empty.  printMaze finds the non-empty part of the maze,
// and prints that.
// This function is called after every move.
func printMaze(m *Maze, oldLocX, oldLocY, newLocX, newLocY int) {
	c := m.glimpsedUpperLeftCorner()
	changed := ipc.SetShift(c)
	// Only print if we are in termbox mode
	if ipc.IsTermboxInited() {
		// If the non-empty part has gotten bigger, print
		// the whole re-shifted maze, otherwise only print
		// the last rooms visited.
		if changed {
			ipc.PrintMaze(m)
		} else {
			ipc.PrintMazeRoom(m, oldLocX, oldLocY)
			ipc.PrintMazeRoom(m, newLocX, newLocY)
		}
		ipc.TermboxSleep(20 * time.Millisecond)
	}
}

// This is the main solving routine.  It will run for
// "count" steps before giving up.  The mazelib.Survey argument
// describes the room that Icarus wakes up in.
// In every room he visits, Icarus follows the same algorithm to decide
// where to go next.  His priorities (highest to lowest):
// 1) Don't walk through walls!
// 2) Don't revisit previously visited rooms.
// 3) If no neighboring rooms meet the above criteria, perform
// a search to find the nearest room that has been "glimpsed" (i.e.
// is adjacent to a previously visited room, with no intervening wall),
// but that has not been visited.  Proceed to that room by the shortest
// path.
// 4) If more than one room meets the criteria in (2) or (3),
// proceed to the one that is in the direction (most to least
// preferred) N, S, E, W.
// Criterion 4 is important because it ensures that in an empty maze
// (one with no internal walls), Icarus performs a systematic scan rather
// than a random walk.
// These criteria imply that Icarus maintains a mental model of the
// parts of the maze that he has visited, so that he knows which rooms
// he has already visited, and so that if necessary he can figure out
// the shortest path to the nearest glimpsed but unvisited room.
func directedWalk(s *mazelib.Survey, count int) bool {
	sa := mazelib.NewSArray(s)
	// Create a "mental maze" for Icarus.  We don't know where Icarus
	// will wake up, so Icarus's maze needs to be 2 times as wide and as
	// high as Daedalus's maze.  Ideally, this would be resizable, but
	// it isn't at this time.
	/*
	iWidth := 40
	iHeight := 30
	*/
	iWidth := 2*viper.GetInt("width")
	iHeight := 2*viper.GetInt("height")
	maze := emptyMaze(iWidth, iHeight)
	// Start in the middle of the maze, so we have room
	// to wander in all directions.
	locX := iWidth / 2
	locY := iHeight / 2
	maze.SetStartPoint(locX, locY)
	oldLocX := locX
	oldLocY := locY
	// Turn-by-turn directions to the nearest unvisited room.  Usually
	// empty, unless we are proceeding to a room identified by
	// criterion 3 above.
	var turns []int
	// i keep track of the number of steps.  After "count" steps, give up.
	for i := 0; i < count; i++ {
		// update current room
		// (note that sa, locX, locY come from previous iteration of loop)
		ar, err := maze.getAugRoom(locX, locY)
		if err != nil {
			return false
		}
		ar.Visited = true
		ar.glimpsed = true
		for _, dir := range mazelib.NSEW {
			if sa.Walls[dir] {
				maze.addWall(locX, locY, dir)
			} else {
				c := mazelib.Dir2Delta[dir]
				x := locX + c.X
				y := locY + c.Y
				ar, err := maze.getAugRoom(x, y)
				if err == nil {
					// neighboring room that is not blocked by
					// a wall, and that is not outside the maze
					ar.glimpsed = true
				}
			}
		}
		maze.icarus.X = locX
		maze.icarus.Y = locY

		printMaze(maze, oldLocX, oldLocY, locX, locY)

		// figure out which direction to go next
		var dir int
		if len(turns) > 0 {
			// if turn-by-turn directions to the nearest unvisited room
			// already exist, use them.
			dir = turns[0]
			turns = turns[1:]
		}
		if dir == 0 { // no such room was found...
			saVisited := mazelib.TrueIfWallOrVisited(maze, locX, locY)
			// dir is the direction of a room that is not walled off, and
			// has not already been visited
			dir = saVisited.FirstUnwalledDirection(true)
		}
		if dir == 0 { // no such room was found...
			// Calculate the turn-by-turn directions to the
			// nearest unvisited room
			turns = maze.turnByTurnToUnvisited(locX, locY, true)
			if len(turns) > 0 {
				dir = turns[0]
				turns = turns[1:]
			}
		}
		if dir == 0 { // no such room was found...
			// Choose a random direction (but in theory this
			// should never happen)
			dir = sa.RandomUnwalledDirection()
		}

		// Move, and then gather info about newly-entered room
		s, err := Move(mazelib.Dir2Move[dir])
		if err != nil && err.Error() != "" {
			// fmt.Println("rw error", err)
			// ipc.Println("rw error", err)
			ipc.PrintMaze(maze)
			return true
		}

		// update location and sa
		oldLocX = locX
		oldLocY = locY
		c := mazelib.Dir2Delta[dir]
		locX += c.X
		locY += c.Y
		sa = mazelib.NewSArray(&s)
	}
	return false
}

// This is an alternate solving routine, originally used for testing.
// It will run for "count" steps.  The mazelib.Survey argument describes
// the room that Icarus wakes up in.
// In every room he visits, Icarus follows the same algorithm to decide
// where to go next.  His priorities (highest to lowest):
// 1) Don't try to walk through walls!
// 2) Try not to visit already-visited rooms.
// 3) If the above criteria can't be obeyed, randomly select
// an accessible room.
func randomWalk(s *mazelib.Survey, count int) bool {
	sa := mazelib.NewSArray(s)
	iWidth := 40
	iHeight := 40
	maze := emptyMaze(iWidth, iHeight)
	locX := iWidth / 2
	locY := iHeight / 2
	for i := 0; i < count; i++ {
		ar, err := maze.getAugRoom(locX, locY)
		if err != nil {
			return false
		}
		ar.Visited = true
		ar.glimpsed = true
		for _, dir := range mazelib.NSEW {
			if sa.Walls[dir] {
				maze.addWall(locX, locY, dir)
			} else {
				c := mazelib.Dir2Delta[dir]
				x := locX + c.X
				y := locY + c.Y
				ar, err := maze.getAugRoom(x, y)
				if err == nil {
					ar.glimpsed = true
				}
			}
		}
		saVisited := mazelib.TrueIfWallOrVisited(maze, locX, locY)
		// dir is the direction of a room that is not walled off, and
		// has not already been visited
		dir := saVisited.RandomUnwalledDirection()
		if dir == 0 { // no such room was found...
			dir = sa.RandomUnwalledDirection()
		}
		s, err := Move(mazelib.Dir2Move[dir])
		if err != nil && err.Error() != "" {
			// ipc.Println("rw error", err)
			return true
		}
		c := mazelib.Dir2Delta[dir]
		locX += c.X
		locY += c.Y
		sa = mazelib.NewSArray(&s)
	}
	return false
}

// TODO: This is where you work your magic
func solveMaze() bool {
	s := awake() // Need to start with waking up to initialize a new maze
	// You'll probably want to set this to a named value and start by figuring
	// out which step to take next

	//TODO: Write your solver algorithm here
	maxSteps := viper.GetInt("max-steps")
	return directedWalk(&s, maxSteps)
}
