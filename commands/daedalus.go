// Copyright © 2015 Steve Francia <spf@spf13.com>.
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//

package commands

import (
	// "errors"
	"fmt"
	"math/rand"
	"net/http"
	"os"
	"os/signal"
	"time"
	// "bytes"
	// "regexp"

	"github.com/gin-gonic/gin"
	// "github.com/golangchallenge/gc6/mazelib"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/sixhebe/gc6/mazelib"
)

// Tracking the current maze being solved

// WARNING: This approach is not safe for concurrent use
// This server is only intended to have a single client at a time
// We would need a different and more complex approach if we wanted
// concurrent connections than these simple package variables
var currentMaze *Maze
var scores []int

// Defining the daedalus command.
// This will be called as 'laybrinth daedalus'
var daedalusCmd = &cobra.Command{
	Use:     "daedalus",
	Aliases: []string{"deadalus", "server"},
	Short:   "Start the laybrinth creator",
	Long: `Daedalus's job is to create a challenging Labyrinth for his opponent
  Icarus to solve.

  Daedalus runs a server which Icarus clients can connect to to solve laybrinths.`,
	Run: func(cmd *cobra.Command, args []string) {
		RunServer()
	},
}

func init() {
	// rand.Seed(time.Now().UTC().UnixNano()) // need to initialize the seed
	rand.Seed(time.Now().UTC().UnixNano()) // need to initialize the seed
	gin.SetMode(gin.ReleaseMode)

	/*
		daedalusCmd.Flags().IntP("width", "x", 15, "width of the laybrinth")
		daedalusCmd.Flags().IntP("height", "y", 10, "height of the laybrinth")

		// Bind viper to these flags so viper can also read them from config, env, etc.
		viper.SetDefault("width", 15)
		viper.SetDefault("height", 10)
		viper.BindPFlag("width", daedalusCmd.Flags().Lookup("width"))
		viper.BindPFlag("height", daedalusCmd.Flags().Lookup("height"))
	*/
	RootCmd.AddCommand(daedalusCmd)
}

// MyGinLogger is based on
// https://github.com/gin-gonic/gin/blob/master/logger.go
// Modified to use mazelib.PrintController for printing
// if verbose is false, nothing will be printed
func MyGinLogger(verbose bool) gin.HandlerFunc {
	return func(c *gin.Context) {
		if verbose && dpc.IsVerbose() {
			start := time.Now()
			path := c.Request.URL.Path
			c.Next()
			end := time.Now()
			latency := end.Sub(start)
			status := c.Writer.Status()
			dpc.Printf("%3d %13v %s\n", status, latency, path)
		}
	}
}

var dpc mazelib.PrintController

// RunServer runs the web server
func RunServer() {
	// Adding handling so that even when ctrl+c is pressed we still print
	// out the results prior to exiting.
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	go func() {
		<-c
		printResults()
		os.Exit(1)
	}()

	verboseStr := viper.GetString("verbose")
	v := mazelib.ParseVerboseFlag(verboseStr)
	if v == 'p' {
		dpc.SetVerbose(true)
	} else if v == 't' {
		dpc.SetVerbose(true)
		dpc.OpenTermbox()
		// Termbox catches all keyboard input, so make sure ^C is
		// routed to channel c
		dpc.NotifyIfTermboxInterrupt(c)
	} else {
		dpc.SetVerbose(false)
	}

	// Using gin-gonic/gin to handle our routing; use our own logger
	r := gin.New()
	r.Use(gin.Recovery())
	// use custom gin logger so that log output is routed through
	// termbox.PrintController
	r.Use(MyGinLogger(false))
	// r.Use(gin.Logger())
	v1 := r.Group("/")
	{
		v1.GET("/awake", GetStartingPoint)
		v1.GET("/move/:direction", MoveDirection)
		v1.GET("/done", End)
	}

	r.Run(":" + viper.GetString("port"))
	dpc.CloseTermbox() // safe to call this even if termbox has not been opened
}

// End ends a session and prints the results.
// Called by Icarus when he has reached
// the number of times he wants to solve the laybrinth.
func End(c *gin.Context) {
	printResults()
	os.Exit(1)
}

// GetStartingPoint initializes a new maze and places Icarus
// in his awakening location
func GetStartingPoint(c *gin.Context) {
	initializeMaze()
	startRoom, err := currentMaze.Discover(currentMaze.Icarus())
	if err != nil {
		fmt.Println("Icarus is outside of the maze. This shouldn't ever happen")
		fmt.Println(err)
		os.Exit(-1)
	}
	// mazelib.PrintMaze(currentMaze)
	// mazelib.PrintMaze2(os.Stdout, currentMaze)
	dpc.PrintMaze(currentMaze)

	c.JSON(http.StatusOK, mazelib.Reply{Survey: startRoom})
}

// MoveDirection is the API response to the /move/:direction address
func MoveDirection(c *gin.Context) {
	var err error

	oldIcarusX, oldIcarusY := currentMaze.Icarus()

	switch c.Param("direction") {
	case "left":
		err = currentMaze.MoveLeft()
	case "right":
		err = currentMaze.MoveRight()
	case "down":
		err = currentMaze.MoveDown()
	case "up":
		err = currentMaze.MoveUp()
	}

	var r mazelib.Reply

	if err != nil {
		r.Error = true
		r.Message = err.Error()
		c.JSON(409, r)
		return
	}

	s, e := currentMaze.LookAround()
	icarusX, icarusY := currentMaze.Icarus()
	dpc.PrintMazeRoom(currentMaze, oldIcarusX, oldIcarusY)
	dpc.PrintMazeRoom(currentMaze, icarusX, icarusY)
	dpc.TermboxSleep(20 * time.Millisecond)
	// time.Sleep(20*time.Millisecond)

	if e != nil {
		if e == mazelib.ErrVictory {
			scores = append(scores, currentMaze.StepsTaken)
			r.Victory = true
			r.Message = fmt.Sprintf("Victory achieved in %d steps", currentMaze.StepsTaken)
			// mazelib.PrintMaze2(os.Stdout, currentMaze)
			dpc.PrintMaze(currentMaze)
			dpc.Println(r.Message)
			dpc.TermboxSleep(3 * time.Second)
		} else {
			r.Error = true
			r.Message = err.Error()
		}
	}

	r.Survey = s

	c.JSON(http.StatusOK, r)
}

func initializeMaze() {
	currentMaze = createMaze()
}

// Print to the terminal the average steps to solution for the current session
func printResults() {
	// time.Sleep(3.*time.Second)
	dpc.CloseTermbox() // safe to call this even if termbox has not been opened
	fmt.Printf("Labyrinth solved %d times with an avg of %d steps\n", len(scores), mazelib.AvgScores(scores))
}

// A simple test maze
func (m *Maze) try1() {
	w := m.Width()
	h := m.Height()
	for x := 0; x < w/2; x++ {
		for y := 0; y < h-x-1; y++ {
			m.addWall(x, y, mazelib.E)
			if y == h/3 {
				// m.addWall(x+1, y, mazelib.S)
			}
		}
	}
}

// calls breadthFirst with a random start point
func (m *Maze) breadthFirstRandom() {
	m.breadthFirst(mazelib.Coordinate{X: -1, Y: -1})
}

// build a maze using a breadth-first approach, which tends
// to create very short corridors.  This is similar to the
// algorithm that http://www.astrolog.org/labyrnth/algrithm.htm
// calls "Growing tree algorithm", the variant where the builder
// always picks the oldest cells added to the list.
// In this implementation, instead of a list, an expanding front
// is used.
func (m *Maze) breadthFirst(start mazelib.Coordinate) {
	c := start
	if c.X < 0 || c.Y < 0 {
		c.X = m.Width() / 2
		c.Y = m.Height() / 2
	}
	m.setAllWalls()
	// holds a set of AugRooms
	type augRoomSet map[*AugRoom]bool
	r := &m.rooms[c.Y][c.X]
	r.Visited = true
	// Initialize curFront ("current front") with the starting room
	curFront := augRoomSet{r: true}
	for len(curFront) > 0 {
		nextFront := make(augRoomSet)
		// Overview:
		// Loop over all rooms in curFront,  and for each room,
		// randomly select an eligible neighbor of one of the rooms,
		// to expand into.
		// When the supply of eligible neighbors
		// is exhausted, move on to the next front.

		// Outer loop: keep trying until supply of eligible neighbors
		// is exhausted.
		for {
			type RoomsAndDir struct {
				room      *AugRoom
				neighbor  *AugRoom
				direction int
			}
			rads := make([]RoomsAndDir, 0, len(curFront)*4)
			// Inner loop: examine each room in curFront and select
			// an eligible neighbor.  If no neighbors available,
			// remove that room from curFront
			for r := range curFront {
				var found bool
				for _, dir := range mazelib.NSEW {
					d := mazelib.Dir2Delta[dir]
					n, err := m.getAugRoom(r.X+d.X, r.Y+d.Y)
					if err != nil || n.Visited {
						continue
					}
					rad := RoomsAndDir{room: r, neighbor: n, direction: dir}
					rads = append(rads, rad)
					found = true
				}
				if !found {
					delete(curFront, r)
				}
			}
			if len(rads) == 0 {
				break
			}
			rad := rads[rand.Int31n(int32(len(rads)))]
			m.removeWall(rad.room.X, rad.room.Y, rad.direction)
			rad.neighbor.Visited = true
			nextFront[rad.neighbor] = true
		}
		curFront = nextFront
	}
	m.clearVisitedFlags()
}

// Call depthFirst with a random start point
func (m *Maze) depthFirstRandom() {
	m.depthFirst(mazelib.Coordinate{X: -1, Y: -1})
}

// Build a maze using a depth-first algorithm, which tends
// to build long corridors.  This is similar to the
// algorithm that http://www.astrolog.org/labyrnth/algrithm.htm
// calls "Recursive backtracker".
// In this implementation, instead of a a stack, the maze itself
// carries the data needed to return to previous positions.
func (m *Maze) depthFirst(start mazelib.Coordinate) {
	c := start
	if c.X < 0 || c.Y < 0 {
		c.X = m.Width() / 2
		c.Y = m.Height() / 2
	}
	m.clearDistances()
	m.setAllWalls()
	var dist int
	for {
		r := &m.rooms[c.Y][c.X]
		r.distance = dist
		var sa mazelib.SArray
		dists := []int{-1, -1, -1, -1, -1}
		for _, dir := range mazelib.NSEW {
			d := mazelib.Dir2Delta[dir]
			n, err := m.getAugRoom(c.X+d.X, c.Y+d.Y)
			if err != nil {
				sa.Walls[dir] = true
			} else if n.distance >= 0 {
				sa.Walls[dir] = true
				dists[dir] = n.distance
			}
		}
		rdir := sa.RandomUnwalledDirection()
		// if rdir > 0, break wall and move forward
		// else move backward
		// if currently in room where dist = 0, break out of loop
		if rdir == 0 {
			if dist == 0 {
				// filled up the maze; all done
				break
			}
			found := false
			for _, dir := range mazelib.NSEW {
				if dists[dir] == dist-1 {
					d := mazelib.Dir2Delta[dir]
					c.X += d.X
					c.Y += d.Y
					dist--
					found = true
					break
				}
			}
			if !found {
				// something went wrong
				break
			}
		} else {
			m.removeWall(c.X, c.Y, rdir)
			d := mazelib.Dir2Delta[rdir]
			c.X += d.X
			c.Y += d.Y
			dist++
		}
	}
	m.clearDistances()
}

// Serrated comb shape
// E-W corridor on the south side
func (m *Maze) southComb() {
	w := m.Width()
	h := m.Height()
	for x := 0; x < w-1; x++ {
		for y := 0; y < h-1; y++ {
			m.addWall(x, y, mazelib.E)
		}
	}
}

// Serrated comb shape
// E-W corridor on the north side
func (m *Maze) northSerr() {
	w := m.Width()
	h := m.Height()
	for y := 1; y < h; y++ {
		for x := 0; x < w-2; x += 3 {
			m.addWall(x, y, mazelib.W)
			m.addWall(x, y, mazelib.N)
			m.addWall(x+2, y, mazelib.E)
			m.addWall(x+2, y, mazelib.N)
		}
	}
}

// Serrated comb shape
// E-W corridor on the north and south sides
func (m *Maze) northSouthSerr() {
	w := m.Width()
	h := m.Height()
	for y := 1; y < h-1; y++ {
		for x := 0; x < w-2; x += 3 {
			m.addWall(x, y, mazelib.W)
			m.addWall(x, y, mazelib.N)
			m.addWall(x, y, mazelib.S)
			m.addWall(x+2, y, mazelib.E)
			m.addWall(x+2, y, mazelib.N)
			m.addWall(x+2, y, mazelib.S)
		}
	}
}

// Serrated comb shape
// E-W corridor on the east side
func (m *Maze) eastSerr() {
	w := m.Width()
	h := m.Height()
	for x := 0; x < w-1; x++ {
		for y := 0; y < h-1; y += 3 {
			m.addWall(x, y, mazelib.N)
			m.addWall(x, y, mazelib.E)
			m.addWall(x, y, mazelib.W)
			m.addWall(x, y+2, mazelib.S)
			m.addWall(x, y+2, mazelib.E)
			m.addWall(x, y+2, mazelib.W)
		}
	}
}

// Simple comb shape
// E-W corridor on the north side
func (m *Maze) northComb() {
	w := m.Width()
	h := m.Height()
	for x := 0; x < w-1; x++ {
		for y := 1; y < h; y++ {
			m.addWall(x, y, mazelib.E)
		}
	}
}

// Simple comb shape
// E-W corridor on the north and south sides
func (m *Maze) northSouthComb() {
	w := m.Width()
	h := m.Height()
	for x := 0; x < w-1; x++ {
		for y := 1; y < h-1; y++ {
			m.addWall(x, y, mazelib.E)
		}
	}
}

// Simple comb shape
// N-S corridor on the west side
func (m *Maze) westComb() {
	w := m.Width()
	h := m.Height()
	for y := 0; y < h-1; y++ {
		for x := 1; x < w; x++ {
			m.addWall(x, y, mazelib.S)
		}
	}
}

// Simple comb shape
// N-S corridor on the east side
func (m *Maze) eastComb() {
	w := m.Width()
	h := m.Height()
	for y := 0; y < h-1; y++ {
		for x := 0; x < w-1; x++ {
			m.addWall(x, y, mazelib.S)
		}
	}
}

var combs = []func(*Maze){
	(*Maze).northComb,
	(*Maze).eastComb,
	(*Maze).southComb,
	(*Maze).westComb,
}

var interesting = []func(*Maze){
	(*Maze).depthFirstRandom,
	(*Maze).eastSerr,
	(*Maze).breadthFirstRandom,
	(*Maze).southComb,
}

// TODO: Write your maze creator function here
func createMaze() *Maze {
	ySize := viper.GetInt("height")
	xSize := viper.GetInt("width")
	showInteresting := viper.GetBool("interesting")

	// TODO: Fill in the maze:
	// You need to insert a startingPoint for Icarus
	// You need to insert an EndingPoint (treasure) for Icarus
	// You need to Add and Remove walls as needed.
	// Use the mazelib.AddWall & mazelib.RmWall to do this

	em := emptyMaze(xSize, ySize)
	err := mazelib.SetOuterWalls(em)
	if err != nil {
		panic(err)
	}

	if showInteresting {
		// Rotate through some mazes that are interesting to look
		// at, but that are not as difficult for Icarus as the
		// comb mazes.
		interesting[len(scores)%len(interesting)](em)
	} else {
		// Rotate through northComb, southComb, eastComb, westComb
		// just to keep Icarus on his toes.
		// combs is an array holding the 4 comb-maze-generating functions.
		combs[len(scores)%len(combs)](em)
	}
	_, err = em.isConsistent()
	if err != nil {
		panic(err)
	}
	// error will be non-nil if the maze is zero-size in one dimension
	// or if there is no empty spot to place Icarus
	err = em.initIcarus()
	if err != nil {
		panic(err)
	}
	// error will be non-nil if the maze is zero-size in one dimension
	// or if there is no empty spot to place the wings
	err = em.initEndpoint()
	if err != nil {
		panic(err)
	}

	_, err = em.isCompliant()
	if err != nil {
		panic(err)
	}
	return em
}
