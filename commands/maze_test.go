package commands

import (
	"testing"

	"gitlab.com/sixhebe/gc6/mazelib"
)

// General note: These tests are testing "package mazelib" functions
// as well as "package commands" functions.  There are no
// tests in the mazelib directory because mazelib works only with
// a maze interface, not with concrete mazes.

// Create and modify a series of mazes.
// Complain if consistency checks fail to spot errors,
// or if they incorrectly complain about errors.
func TestIsConsistent(t *testing.T) {
	var ok bool
	var err error

	// Mazes with 0 width or height are inconsistent.
	m0y := emptyMaze(1, 0)
	ok, err = m0y.isConsistent()
	if ok {
		t.Errorf("Expected error")
	}
	m0x := emptyMaze(0, 1)
	ok, err = m0x.isConsistent()
	if ok {
		t.Errorf("Expected error")
	}

	// 1x1 maze; generate and detect outer walls
	m11 := emptyMaze(1, 1)
	ok, err = m11.isConsistent()
	if !ok {
		t.Errorf("Unexpected error %v", err)
	}
	ok, err = mazelib.AreOuterWallsSet(m11)
	if ok {
		t.Errorf("Expected error")
	}
	err = mazelib.SetOuterWalls(m11)
	if err != nil {
		t.Errorf("Unexpected error %v", err)
	}
	ok, err = mazelib.AreOuterWallsSet(m11)
	if !ok {
		t.Errorf("Unexpected error %v", err)
	}

	// 2x2 maze; generate and detect outer walls.
	// Generate and check for inconsistent (one-way) and consistent walls.
	m22 := emptyMaze(2, 2)
	err = mazelib.SetOuterWalls(m22)
	if err != nil {
		t.Errorf("Unexpected error %v", err)
	}
	ok, err = m22.isConsistent()
	if !ok {
		t.Errorf("Unexpected error %v", err)
	}
	ok, err = mazelib.AreOuterWallsSet(m22)
	if !ok {
		t.Errorf("Unexpected error %v", err)
	}
	// Created a one-way wall
	m22.rooms[0][0].Walls.Right = true
	ok, err = m22.isConsistent()
	if ok {
		t.Errorf("Expected error")
	}
	// Created the other side of the wall
	m22.rooms[0][1].Walls.Left = true
	ok, err = m22.isConsistent()
	if !ok {
		t.Errorf("Unexpected error %v", err)
	}
	// Created a one-way wall
	m22.rooms[0][0].Walls.Bottom = true
	ok, err = m22.isConsistent()
	if ok {
		t.Errorf("Expected error")
	}
	// Created the other side of the wall
	m22.rooms[1][0].Walls.Top = true
	ok, err = m22.isConsistent()
	if !ok {
		t.Errorf("Unexpected error %v", err)
	}
	m22.rooms[0][1].X = 0
	ok, err = m22.isConsistent()
	if ok {
		t.Errorf("Expected error")
	}
	m22.rooms[0][1].X = 1
	ok, err = m22.isConsistent()
	if !ok {
		t.Errorf("Unexpected error %v", err)
	}
	m22.rooms[0][1].Y = 1
	ok, err = m22.isConsistent()
	if ok {
		t.Errorf("Expected error")
	}
	m22.rooms[0][1].Y = 0
	ok, err = m22.isConsistent()
	if !ok {
		t.Errorf("Unexpected error %v", err)
	}

	// 3x3 maze.
	// Same checks as 2x2 maze, but also make sure startpoints and
	// endpoints are properly set, and check whether unreachable rooms
	// have been generated.
	m33 := emptyMaze(3, 3)
	err = mazelib.SetOuterWalls(m33)
	if err != nil {
		t.Errorf("Unexpected error %v", err)
	}
	ok, err = m33.isConsistent()
	if !ok {
		t.Errorf("Unexpected error %v", err)
	}
	ok, err = mazelib.AreOuterWallsSet(m33)
	if !ok {
		t.Errorf("Unexpected error %v", err)
	}
	for _, dir := range mazelib.NSEW {
		m33.addWall(1, 1, dir)
		ok, err = m33.isConsistent()
		if !ok {
			t.Errorf("Unexpected error %v", err)
		}
	}
	m33.removeWall(0, 0, mazelib.W)
	ok, err = m33.isConsistent()
	if !ok {
		t.Errorf("Unexpected error %v", err)
	}
	ok, err = mazelib.AreOuterWallsSet(m33)
	if ok {
		t.Errorf("Expected error")
	}
	m33.addWall(0, 0, mazelib.W)
	ok, err = m33.isConsistent()
	if !ok {
		t.Errorf("Unexpected error %v", err)
	}
	ok, err = mazelib.AreOuterWallsSet(m33)
	if !ok {
		t.Errorf("Unexpected error %v", err)
	}
	ok, err = m33.areStartAndEndConsistent()
	if err == nil {
		t.Errorf("Expected error")
	}
	err = m33.SetStartPoint(0, 0)
	if err != nil {
		t.Errorf("Unexpected error %v", err)
	}
	err = m33.SetTreasure(0, 0)
	if err == nil {
		t.Errorf("Expected error")
	}
	ok, err = m33.areStartAndEndConsistent()
	if err == nil {
		t.Errorf("Expected error")
	}
	err = m33.SetTreasure(1, 1)
	if err != nil {
		t.Errorf("Unexpected error %v", err)
	}
	ok, err = m33.areStartAndEndConsistent()
	if err != nil {
		t.Errorf("Unexpected error %v", err)
	}
	// Should complain because 1,1 is a completely enclosed room,
	// unreachable from the rest of the maze
	ok, err = m33.isCompliant()
	if err == nil {
		t.Errorf("Expected error")
	}
	// Now 1,1 is reachable
	m33.removeWall(1, 1, mazelib.N)
	ok, err = m33.isCompliant()
	if err != nil {
		t.Errorf("Unexpected error %v", err)
	}
}

func TestTurnByTurn(t *testing.T) {
	N := mazelib.N
	S := mazelib.S
	E := mazelib.E
	W := mazelib.W
	dirs := []string{"", "N", "S", "E", "W"}
	type Coord mazelib.Coordinate
	type Case struct {
		start     Coord   // starting point
		unvisited []Coord // possible end points (these are set as unvisited)
		nsBias    bool    // north-south bias flag
		expected  []int   // expected turn-by-turn directions
	}
	cases := []Case{
		{Coord{1, 1}, []Coord{{1, 2}}, true, []int{N, E, S, S, W}},
		{Coord{1, 1}, []Coord{{1, 2}}, false, []int{N, E, S, S, W}},
		{Coord{0, 0}, []Coord{{2, 2}}, true, []int{E, E, S, S}},
		{Coord{0, 0}, []Coord{{2, 2}}, false, []int{E, E, S, S}},
		{Coord{0, 0}, []Coord{{1, 2}, {2, 1}}, true, []int{S, S, E}},
		{Coord{0, 0}, []Coord{{1, 2}, {2, 1}}, false, []int{E, E, S}},
		{Coord{0, 0}, []Coord{{1, 2}, {2, 2}}, true, []int{S, S, E}},
		{Coord{0, 0}, []Coord{{1, 2}, {2, 2}}, false, []int{S, S, E}},
		{Coord{1, 2}, []Coord{{1, 1}}, true, []int{E, N, N, W, S}},
		{Coord{0, 1}, []Coord{{1, 1}, {2, 2}}, true, []int{N, E, S}},
		{Coord{0, 1}, []Coord{{1, 1}, {2, 2}}, false, []int{N, E, S}},
		{Coord{2, 1}, []Coord{{0, 1}}, true, []int{N, W, W, S}},
	}
	for i, c := range cases {
		m := emptyMaze(3, 3)
		m.addWall(1, 1, E)
		m.addWall(1, 1, W)
		m.addWall(1, 1, S)
		for x := 0; x < m.Width(); x++ {
			for y := 0; y < m.Height(); y++ {
				r := &m.rooms[y][x]
				r.Visited = true
				r.glimpsed = true
			}
		}
		for _, u := range c.unvisited {
			m.rooms[u.Y][u.X].Visited = false
		}
		tbt := m.turnByTurnToUnvisited(c.start.X, c.start.Y, c.nsBias)
		if len(tbt) == len(c.expected) {
			for j := 0; j < len(tbt); j++ {
				if tbt[j] != c.expected[j] {
					// note that case and turn are zero-indexed
					t.Errorf("TurnByTurn case %d turn %d expected %s got %s", i, j, dirs[c.expected[j]], dirs[tbt[j]])
				}
			}
		} else {
			// note that case is zero-indexed
			t.Errorf("TurnByTurn case %d expected %d turns got %d", i, len(c.expected), len(tbt))
		}
	}
}
