// Copyright © 2015 Steve Francia <spf@spf13.com>.
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//

package commands

import (
	"errors"
	"fmt"
	"math/rand"

	"gitlab.com/sixhebe/gc6/mazelib"
)

// AugRoom stands for Augmented Room; it holds supplemental
// info used by icarus to keep track of his location in the maze.
// Design discussion: I could have added this supplemental info
// to the existing mazelib.Room struct instead of creating an
// additional struct.  But I wanted, as an exercise, to see
// how to augment a data structure in Go.  Sometimes the original
// struct is in an external library and cannot be modified.
type AugRoom struct {
	mazelib.Room
	glimpsed bool // has room been glimpsed (i.e. inferred because of the
	// the absence of a wall as seen from a visited room.
	distance int // used in the turn-by-turn function
	mazelib.Coordinate
}

// Maze holds the maze used by icarus and daedalus
type Maze struct {
	rooms      [][]AugRoom
	start      mazelib.Coordinate
	end        mazelib.Coordinate
	icarus     mazelib.Coordinate
	StepsTaken int
}

// Return a room from the maze
func (m *Maze) getAugRoom(x, y int) (*AugRoom, error) {
	if x < 0 || y < 0 || x >= m.Width() || y >= m.Height() {
		return &AugRoom{}, errors.New("room outside of maze boundaries")
	}
	return &m.rooms[y][x], nil
}

// GetRoom is part of the MazeI interface; it returns the room at
// the given x,y position, or an error if x,y are outside of the
// maze boundaries.
func (m *Maze) GetRoom(x, y int) (*mazelib.Room, error) {
	ar, err := m.getAugRoom(x, y)
	return &ar.Room, err
}

// Width is part of the MazeI interface.  It returns the width of the maze.
func (m *Maze) Width() int { return len(m.rooms[0]) }

// Height is part of the MazeI interface.  It returns the width of the maze.
func (m *Maze) Height() int { return len(m.rooms) }

// Icarus is part of the MazeI interface.
// It returns Icarus's current position.
func (m *Maze) Icarus() (x, y int) {
	return m.icarus.X, m.icarus.Y
}

// SetStartPoint is part of the MazeI interface.
// It sets the location where Icarus will awake.
func (m *Maze) SetStartPoint(x, y int) error {
	r, err := m.GetRoom(x, y)

	if err != nil {
		return err
	}

	if r.Treasure {
		return errors.New("can't start in the treasure")
	}

	r.Start = true
	r.Visited = true
	m.icarus = mazelib.Coordinate{X: x, Y: y}
	m.start = mazelib.Coordinate{X: x, Y: y}
	return nil
}

// SetTreasure is part of the MazeI interface.
// It sets the location of the treasure for a given maze.
func (m *Maze) SetTreasure(x, y int) error {
	r, err := m.GetRoom(x, y)

	if err != nil {
		return err
	}

	if r.Start {
		return errors.New("can't have the treasure at the start")
	}

	r.Treasure = true
	m.end = mazelib.Coordinate{X: x, Y: y}
	return nil
}

// LookAround is part of the MazeI interface.
// Given Icarus's current location, survey that room.
// Will return ErrVictory if Icarus is at the treasure.
func (m *Maze) LookAround() (mazelib.Survey, error) {
	if m.end.X == m.icarus.X && m.end.Y == m.icarus.Y {
		// fmt.Printf("Victory achieved in %d steps \n", m.StepsTaken)
		return mazelib.Survey{}, mazelib.ErrVictory
	}

	return m.Discover(m.icarus.X, m.icarus.Y)
}

// Discover is part of the MazeI interface.
// Given x,y, return the walls of the room.
// Will return error if x,y is outside of the maze
func (m *Maze) Discover(x, y int) (mazelib.Survey, error) {
	if r, err := m.GetRoom(x, y); err != nil {
		return mazelib.Survey{}, nil
	} else {
		// ignore golint's plea to move this out of the else block;
		// r is only defined inside the if/else block
		return r.Walls, nil
	}
}

// MoveLeft is part of the MazeI interface.
// Moves Icarus's position left one step.
// Will not permit moving through walls or out of the maze.
func (m *Maze) MoveLeft() error {
	s, e := m.LookAround()
	if e != nil {
		return e
	}
	if s.Left {
		return errors.New("Can't walk through walls")
	}

	x, y := m.Icarus()
	r, err := m.GetRoom(x-1, y)
	if err != nil {
		return err
	}

	r.Visited = true
	m.icarus = mazelib.Coordinate{X: x - 1, Y: y}
	m.StepsTaken++
	return nil
}

// MoveRight is part of the MazeI interface.
// Moves Icarus's position right one step.
// Will not permit moving through walls or out of the maze.
func (m *Maze) MoveRight() error {
	s, e := m.LookAround()
	if e != nil {
		return e
	}
	if s.Right {
		return errors.New("Can't walk through walls")
	}

	x, y := m.Icarus()
	r, err := m.GetRoom(x+1, y)
	if err != nil {
		return err
	}

	r.Visited = true
	m.icarus = mazelib.Coordinate{X: x + 1, Y: y}
	m.StepsTaken++
	return nil
}

// MoveUp is part of the MazeI interface.
// Moves Icarus's position up one step.
// Will not permit moving through walls or out of the maze.
func (m *Maze) MoveUp() error {
	s, e := m.LookAround()
	if e != nil {
		return e
	}
	if s.Top {
		return errors.New("Can't walk through walls")
	}

	x, y := m.Icarus()
	r, err := m.GetRoom(x, y-1)
	if err != nil {
		return err
	}

	r.Visited = true
	m.icarus = mazelib.Coordinate{X: x, Y: y - 1}
	m.StepsTaken++
	return nil
}

// MoveDown is part of the MazeI interface.
// Moves Icarus's position down one step.
// Will not permit moving through walls or out of the maze.
func (m *Maze) MoveDown() error {
	s, e := m.LookAround()
	if e != nil {
		return e
	}
	if s.Bottom {
		return errors.New("Can't walk through walls")
	}

	x, y := m.Icarus()
	r, err := m.GetRoom(x, y+1)
	if err != nil {
		return err
	}

	r.Visited = true
	m.icarus = mazelib.Coordinate{X: x, Y: y + 1}
	m.StepsTaken++
	return nil
}

// Checks whether the maze complies with the rules, by
// calling various consistency-check functions.
// Checks:
// 1) Outer walls are set
// 2) Topology is consistent (no one-way walls)
// 3) Start and end points are defined properly
// 4) All rooms of the maze are reachable from all other rooms
// Returns non-nil error if false
func (m *Maze) isCompliant() (bool, error) {
	// _, err := m.areOuterWallsSet()
	_, err := mazelib.AreOuterWallsSet(m)
	if err != nil {
		return false, err
	}
	_, err = m.isConsistent()
	if err != nil {
		return false, err
	}
	_, err = m.areStartAndEndConsistent()
	if err != nil {
		return false, err
	}
	_, err = m.areAllPointsReachable()
	if err != nil {
		return false, err
	}
	return true, nil
}

// Checks if all rooms in the maze are reachable (that is,
// that some rooms aren't completely walled off from the rest).
// Returns non-nil error if false
func (m *Maze) areAllPointsReachable() (bool, error) {
	// Holds a set of AugRooms.
	type augRoomSet map[*AugRoom]bool
	ar0, err := m.getAugRoom(m.start.X, m.start.Y)
	if err != nil {
		ar0, err = m.getAugRoom(0, 0)
	}
	if err != nil {
		return false, err
	}
	wh := m.Width() * m.Height()
	visited := make(augRoomSet, wh)
	visited[ar0] = true
	prevFrontier := augRoomSet{ar0: true}
	for {
		frontier := make(augRoomSet, 2*len(prevFrontier)+1)
		for ar := range prevFrontier {
			ns := m.visitableNeighbors(ar.X, ar.Y)
			for _, n := range ns {
				if visited[n] {
					continue
				}
				visited[n] = true
				frontier[n] = true
			}
		}
		if len(frontier) == 0 {
			break
		}
		prevFrontier = frontier
	}
	if len(visited) != wh {
		return false, fmt.Errorf("Expected %d reachable rooms, found %d", wh, len(visited))
	}
	return true, nil
}

// The end point is defined in two places: in the maze structure,
// and as a flag in a room.  This function checks that the two
// definitions are consistent.  Likewise for the start point.
// Returns non-nil error if false
func (m *Maze) areStartAndEndConsistent() (bool, error) {
	start, err := m.getAugRoom(m.start.X, m.start.Y)
	if err != nil {
		return false, fmt.Errorf("Start point wrongly set: %s", err)
	}
	if !start.Start {
		return false, errors.New("Starting room's Start flag not set")
	}
	end, err := m.getAugRoom(m.end.X, m.end.Y)
	if err != nil {
		return false, fmt.Errorf("End point wrongly set: %s", err)
	}
	if !end.Treasure {
		return false, errors.New("Ending room's Treasure flag not set")
	}
	ySize := len(m.rooms)
	for y := 0; y < ySize; y++ {
		xSize := len(m.rooms[y])
		for x := 0; x < xSize; x++ {
			ar := &m.rooms[y][x]
			if (x != m.start.X || y != m.start.Y) && ar.Start {
				return false, fmt.Errorf("Room at %d,%d has start point incorrectly set", x, y)
			}
			if (x != m.end.X || y != m.end.Y) && ar.Treasure {
				return false, fmt.Errorf("Room at %d,%d has end point incorrectly set", x, y)
			}
		}
	}
	if start.X == end.X && start.Y == end.Y {
		return false, errors.New("Start and end points are co-located")
	}

	return true, nil
}

// returns non-nil error if false
// check that height and width are > 0
// check that all rows are the same length
// check that there are no one-way walls
// check that room Coordinate value is correct.
// Maze.start, Maze.end, and Maze.icarus are not checked
func (m *Maze) isConsistent() (bool, error) {
	ySize := len(m.rooms)
	if ySize == 0 {
		return false, errors.New("Height (ySize) is zero rooms")
	}
	xs := -1
	for y := 0; y < ySize; y++ {
		xSize := len(m.rooms[y])
		if xs < 0 {
			xs = xSize
			if xs == 0 {
				return false, errors.New("Width (xSize) is zero rooms")
			}
		}
		if xs != xSize {
			return false, errors.New("Width (xSize) is inconsistent from row to row")
		}
	}
	ok, err := mazelib.IsTopologyConsistent(m)
	if !ok {
		return false, err
	}
	for y := 0; y < ySize; y++ {
		xSize := len(m.rooms[y])
		for x := 0; x < xSize; x++ {
			r0 := &m.rooms[y][x]
			if r0.X != x {
				return false, fmt.Errorf("Room at %d,%d has incorrect x value %d", x, y, r0.X)
			}
			if r0.Y != y {
				return false, fmt.Errorf("Room at %d,%d has incorrect y value %d", x, y, r0.Y)
			}
		}
	}
	return true, nil
}

// In the room at location x,y, create the wall in direction dir.
// For more details, see comments for setWall().
func (m *Maze) addWall(x, y, dir int) error {
	return m.setWall(x, y, dir, true)
}

// In the room at location x,y, remove the wall in direction dir.
// For more details, see comments for setWall().
func (m *Maze) removeWall(x, y, dir int) error {
	return m.setWall(x, y, dir, false)
}

// In the room at location x,y, add/remove the wall in direction dir.
// (whether to add the wall or remove it is determined by "value").
// This function is designed to prevent the creation of one-way
// walls: besides adding/removing the wall in the room at x,y,
// it adds/removes the corresponding wall in the appropriate neighboring
// cell.
func (m *Maze) setWall(x, y, dir int, value bool) error {
	ySize := len(m.rooms)
	if y < 0 || y >= ySize {
		return errors.New("setWall: y value out of range")
	}
	xSize := len(m.rooms[y])
	if x < 0 || x >= xSize {
		return errors.New("setWall: x value out of range")
	}
	if dir < mazelib.NSEW[0] || dir > mazelib.NSEW[3] {
		return errors.New("setWall: dir value out of range")
	}
	r0 := &m.rooms[y][x]
	switch dir {
	case mazelib.N:
		r0.Walls.Top = value
		if y > 0 {
			r1 := &m.rooms[y-1][x]
			r1.Walls.Bottom = value
		}
	case mazelib.S:
		r0.Walls.Bottom = value
		if y < ySize-1 {
			r1 := &m.rooms[y+1][x]
			r1.Walls.Top = value
		}
	case mazelib.E:
		r0.Walls.Right = value
		if x < xSize-1 {
			r1 := &m.rooms[y][x+1]
			r1.Walls.Left = value
		}
	case mazelib.W:
		r0.Walls.Left = value
		if x > 0 {
			r1 := &m.rooms[y][x-1]
			r1.Walls.Right = value
		}
	}
	return nil
}

func (m *Maze) randomPoint() (c mazelib.Coordinate) {
	c.X = int(rand.Int31n(int32(m.Width())))
	c.Y = int(rand.Int31n(int32(m.Height())))
	return c
}

// Set the start point at a random location in the maze.
// If the location is accidently chosen to be the
// same as the end point point, try again.
func (m *Maze) initIcarus() error {
	for i := 0; i < 20; i++ {
		c := m.randomPoint()
		// most likely cause of error: start point accidently same as
		// end point
		err := m.SetStartPoint(c.X, c.Y)
		if err == nil {
			return nil
		}
	}
	return errors.New("setIcarus: Couldn't find an empty spot.  Maze too small?")
}

// Set the endpoint (i.e. the wing/treasure location) at a random
// location in the maze.  If the location is accidently chosen to be the
// same as the start point, try again.
func (m *Maze) initEndpoint() error {
	for i := 0; i < 20; i++ {
		c := m.randomPoint()
		// most likely cause of error: end point accidently same as
		// start point
		err := m.SetTreasure(c.X, c.Y)
		if err == nil {
			return nil
		}
	}
	return errors.New("initEndpoint: Couldn't find an empty spot.  Maze too small?")
}

func (m *Maze) clearDistances() {
	for y := 0; y < m.Height(); y++ {
		for x := 0; x < len(m.rooms[y]); x++ {
			m.rooms[y][x].distance = -1
		}
	}
}

func (m *Maze) clearVisitedFlags() {
	for y := 0; y < m.Height(); y++ {
		for x := 0; x < len(m.rooms[y]); x++ {
			m.rooms[y][x].Visited = false
		}
	}
}

// Finds upper-left corner of bounding box of active area of maze
// ("active area" means all rooms that have at least been glimpsed).
func (m *Maze) glimpsedUpperLeftCorner() mazelib.Coordinate {
	c := mazelib.Coordinate{X: -1, Y: -1}
	for y := 0; y < m.Height(); y++ {
		for x := 0; x < len(m.rooms[y]); x++ {
			ar := m.rooms[y][x]
			if !ar.glimpsed && !ar.Visited {
				continue
			}
			if c.X < 0 || c.X > x {
				c.X = x
			}
			if c.Y < 0 || c.Y > y {
				c.Y = y
			}
		}
	}
	return c
}

// find all of the neighbors of the room at x,y that meet
// the criteria: 1) there is no wall in the way; 2) the
// neighboring room exists (is not outside the maze).
func (m *Maze) visitableNeighbors(x, y int) []*AugRoom {
	var ars []*AugRoom
	ar, err := m.getAugRoom(x, y)
	if err != nil {
		return ars
	}
	sa := mazelib.NewSArray(&ar.Walls)
	for _, dir := range mazelib.NSEW {
		if sa.Walls[dir] {
			continue
		}
		d := mazelib.Dir2Delta[dir]
		ar, err := m.getAugRoom(x+d.X, y+d.Y)
		if err == nil {
			ars = append(ars, ar)
		}
	}
	return ars
}

// returns an ordered list (slice) of directions (NSEW) for going from
// x,y to the nearest unvisited but glimpsed room.
// This is a fairly complicated algorithm.
func (m *Maze) turnByTurnToUnvisited(x, y int, nsBias bool) []int {
	// Clear out information that will be used by this algorithm.
	m.clearDistances()
	var dirs []int
	ar, err := m.getAugRoom(x, y)
	if err != nil {
		return dirs
	}
	ar.distance = 0
	dist := 0
	// Start with the initial room (given in the arguments).
	// Form an expanding frontier of neighboring rooms that are
	// all equidistant from the initial room.
	prevFrontierRooms := map[*AugRoom]bool{ar: true}
	// Keep going until an unvisited room is found, or we run out of rooms.
	for {
		dist++
		frontierRooms := make(map[*AugRoom]bool, 2*len(prevFrontierRooms)+1)
		var foundUnvisitedRoom bool
		// Loop over rooms in the previous frontier.
		// Find new rooms that have at least been glimpsed,
		// that are visitable (not walled off from the current room),
		// and that are not actually closer to the initial point.
		// If any such new rooms are found, store the current distance
		// in the AugRoom structure,
		// and add these rooms to a new list of frontier rooms.
		for ar := range prevFrontierRooms {
			neighbors := m.visitableNeighbors(ar.X, ar.Y)
			for _, n := range neighbors {
				if !n.glimpsed {
					continue
				}
				if n.distance > -1 && n.distance < dist {
					continue
				}
				n.distance = dist
				frontierRooms[n] = true
				if !n.Visited {
					foundUnvisitedRoom = true
				}
			}
		}
		prevFrontierRooms = frontierRooms
		if foundUnvisitedRoom {
			break
		}
		if len(prevFrontierRooms) == 0 {
			// Ran out of places to look before finding any
			// unvisited rooms; give up (return an empty turn-by-turn list)
			return dirs
		}
	}

	// At this point we have found one or more rooms that meet the
	// criteria (previously glimpsed, but not previously visited).
	// Of these, find the one that is closest to the initial room in
	// an east-west distance.
	var minAbsX = -1
	var minXAR *AugRoom
	var minAbsY = -1
	var minYAR *AugRoom

	for ar := range prevFrontierRooms {
		if ar.Visited {
			continue
		}
		ax := ar.X - x
		ay := ar.Y - y
		if ax < 0 {
			// Bias towards east by giving west a higher number
			ax = -2*ax + 1
		} else {
			ax = 2 * ax
		}
		if ay < 0 {
			ay = -2 * ay
		} else {
			// Bias towards south by giving north a higher number
			ay = 2*ay + 1
		}
		if minAbsX < 0 || minAbsX > ax {
			minAbsX = ax
			minXAR = ar
		}
		if minAbsY < 0 || minAbsY > ay {
			minAbsY = ay
			minYAR = ar
		}
	}

	if nsBias {
		ar = minXAR
	} else {
		ar = minYAR
	}

	// Starting from the room that was just found, work backwards
	// to the initial room, by following the distance gradient.
	// Store turn-by-turn directions in the "dirs" array, keeping in
	// mind that we want the direction that would lead from the initial
	// room to the final room, not the other way around.
	for ar.distance > 0 {
		neighbors := m.visitableNeighbors(ar.X, ar.Y)
		var found bool
		for _, n := range neighbors {
			// look for the correct distance gradient
			if n.distance == ar.distance-1 {
				found = true
				var dir int
				if ar.X == n.X+1 {
					dir = mazelib.E
				} else if ar.X == n.X-1 {
					dir = mazelib.W
				} else if ar.Y == n.Y+1 {
					dir = mazelib.S
				} else if ar.Y == n.Y-1 {
					dir = mazelib.N
				} else {
					return []int{}
				}
				// append because it is easy; really should prepend because
				// we will be proceeding in the other direction.  This
				// is handled below.
				dirs = append(dirs, dir)
				ar = n
				break
			}
		}
		if !found {
			return []int{}
		}
	}
	// We must reverse dirs, since it is in backwards order to what we need.
	// Copied from http://stackoverflow.com/questions/19239449/how-do-i-reverse-an-array-in-go
	for i, j := 0, len(dirs)-1; i < j; i, j = i+1, j-1 {
		dirs[i], dirs[j] = dirs[j], dirs[i]
	}

	return dirs
}

func (m *Maze) setAllWalls() {
	h := m.Height()
	w := m.Width()
	for y := 0; y < h; y++ {
		for x := 0; x < w; x++ {
			m.rooms[y][x].Walls = mazelib.Survey{
				Top:    true,
				Right:  true,
				Bottom: true,
				Left:   true,
			}
		}
	}
}

// Creates a maze without any walls
// Good starting point for additive algorithms
func emptyMaze(xSize, ySize int) *Maze {
	z := Maze{}
	z.icarus.X = -1
	z.icarus.Y = -1
	z.start.X = -1
	z.start.Y = -1
	z.end.X = -1
	z.end.Y = -1

	z.rooms = make([][]AugRoom, ySize)
	for y := 0; y < ySize; y++ {
		z.rooms[y] = make([]AugRoom, xSize)
		for x := 0; x < xSize; x++ {
			var ar AugRoom
			ar.Coordinate = mazelib.Coordinate{X: x, Y: y}
			z.rooms[y][x] = ar
		}
	}

	return &z
}

// Creates a maze with all walls
// Good starting point for subtractive algorithms
func fullMaze(xSize, ySize int) *Maze {
	z := emptyMaze(xSize, ySize)
	z.setAllWalls()

	return z
}
